$(function() {
    $('#genLink').submit(function() {
        var data = $(this).serialize();

        $.ajax({
            url: 'includes/ajax/generate.php',
            type: "POST",
            dataType: "jsonp",
            data: data,
            beforeSend: function() {
                $('.lead').empty('loading ...');
            },
            success: function(data) {
                if(data.status == 'success') {
                    $('.lead').html('Shortened URL: <strong>' + data.response +'</strong>');
                } else {
                    alert(data.response);
                }
            },
            error: function() {
                alert('An error happened. Try again later');
            },
            complete: function() {}
        });

        return false;

    });
});