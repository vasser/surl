<?php
require_once 'includes/config.php';
require_once ROOT . 'includes/autoload.php';

$link = base64_decode($_GET['link']);
$link = Func::getRequestStr($link);
$token = Func::getRequestStr($_GET['token']);

$surl = new Surl();

$array = array(
    'token' => $token,
    'url' => $link,
    'status' => '',
    'response' => array()
);

if(!isset($link) || empty($link)) {
    $error[] = 'No link';
}
if(Func::isUrl($link) === false) {
    $error[] = 'Invalid link';
}
if(!isset($token) || empty($token)) {
    $error[] = 'No token';
}
if($surl->checkToken($token) === false) {
    $error[] = 'Invalid token';
}

if(empty($error) || !isset($error)) {
    $enc = $surl->saveUrl($link);
    if ($enc === false) {
        $error = 'An error happened';
    } else {
        $response = $enc;
    }
}

if(isset($error) && is_array($error)) {
    $array['status'] = 'error';
    $array['response'] = $error;
} else {
    $array['status'] = 'success';
    $array['response'] = $enc;
}

echo json_encode($array);