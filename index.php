<?php
require_once 'includes/config.php';
require_once ROOT . 'includes/autoload.php';

$link = Func::getRequestStr($_GET['link']);
$ua = Func::getRequestStr($_SERVER['HTTP_USER_AGENT']);

if(isset($link) && !empty($link)) {

    $surl = new Surl();
    $url = $surl->decodeUrl($link);

    $id_url = $url['id'];

    $stat = new Stat($ua, Func::getIp(), $id_url);
    $stat->updateLog();

    header('Location: ' + $url['url']);
    die;

} else {
    $auth = new Login();

    if($auth->_Init === true) {
        $tpl = 'generate';
        $title = 'generate short url';
    } else {
        $tpl = 'login';
        $title = 'login please';
    }
    Func::loadTpl($tpl, $title);
}