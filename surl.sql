-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 29, 2014 at 04:54 AM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `surl`
--

-- --------------------------------------------------------

--
-- Table structure for table `surl_links`
--

CREATE TABLE IF NOT EXISTS `surl_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `surl` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table contains links pairs: full link - shortened' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `surl_stats`
--

CREATE TABLE IF NOT EXISTS `surl_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_url` int(11) NOT NULL,
  `visits` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_url` (`id_url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Total count of redirects' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `surl_stats_adv`
--

CREATE TABLE IF NOT EXISTS `surl_stats_adv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_url` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `browser` varchar(11) NOT NULL,
  `os` varchar(255) NOT NULL,
  `country` varchar(3) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_url` (`id_url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table with the advanced info about redirects: IP, browser, OS etc' AUTO_INCREMENT=1 ;

--
-- Triggers `surl_stats_adv`
--
DROP TRIGGER IF EXISTS `update_stats`;
DELIMITER //
CREATE TRIGGER `update_stats` AFTER INSERT ON `surl_stats_adv`
 FOR EACH ROW BEGIN
	IF (
        NOT EXISTS (SELECT `id`
                    FROM `surl_stats`
                    WHERE `surl_stats`.`id_url` = NEW.`id_url`)
    )
	THEN
		INSERT INTO `surl_stats` (`id_url`, `visits`)
			VALUES (NEW.`id_url`, '1');
	ELSE
		UPDATE `surl_stats`
			SET `surl_stats`.`visits` = `surl_stats`.`visits` + 1
			WHERE `surl_stats`.`id_url` = NEW.`id_url`;
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `surl_users`
--

CREATE TABLE IF NOT EXISTS `surl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
