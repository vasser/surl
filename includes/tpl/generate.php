<style>
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #eee;
    }

    .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin .checkbox {
        font-weight: normal;
    }
    .form-signin .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
    }
    .form-signin .form-control:focus {
        z-index: 2;
    }
    .form-signin input[type="email"] {
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }
    .form-signin input[type="password"] {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
    .form-signin button {
        margin-top: 10px;
    }
</style>

<?php $auth = new Login(); ?>

<form id="genLink" class="form-signin" role="form">
    <h2 class="form-signin-heading">Generate short URL</h2>
    <input type="hidden" name="user" value="<?php echo $_SESSION['userLoginData'] ?>">
    <input type="hidden" name="token" value="<?php echo Surl::setToken() ?>">
    <input type="text" name="url" class="form-control" placeholder="http://" required autofocus>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Generate</button>
</form>
<p class="lead"></p>
<p>Your token to short URL's remotely: <span class="badge"><?php echo $auth->_user['token'] ?></span></p>