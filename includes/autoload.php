<?php
include ROOT . 'includes/classes/Autoload.class.php';
spl_autoload_register(array('Autoload', 'load'));
Autoload::registerPath(ROOT . 'includes/classes/');
