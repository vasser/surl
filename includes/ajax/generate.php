<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/SURL/includes/config.php';
    require_once ROOT . 'includes/autoload.php';

    $callback = Func::getRequestStr($_REQUEST['callback']);
    $token = Func::getRequestStr($_POST['token']);
    $user = Func::getRequestStr($_POST['user']);
    $url = Func::getRequestStr($_POST['url']);

    $surl = new Surl();
    $status = 'error';

    if(Func::isUrl($url) === true) {
        $enc = $surl->saveUrl($url);
        if($enc === false) {
            $response = 'An error happened';
        } else {
            $status = 'success';
            $response = $enc;
        }
    } else {
        $response = 'You pasted not an URL';
    }

    $arr = array(status => $status, response => $response);
    $arr = json_encode($arr);
    echo $callback."(".$arr.")";
}
?>