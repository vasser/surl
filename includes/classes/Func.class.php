<?php
/**
 * Here we get set of functions
 *
 * @author vasser
 */
class Func {

    /**
     * Safety function to avoid exploits, injections etc
     * @param type $val
     * @return type
     */
    static function getRequestStr($val) {
        $val = strip_tags($val);
        $val = stripslashes($val);
        $val = trim($val);
        $val = htmlspecialchars($val, ENT_QUOTES);
        return $val;
    }

    /**
     * Primitive url validation
     * Here we should also check for XSS exploits, they can be saved in base64, not only in pure JS
     * Also url lenght and so on
     * @param type $url
     * @return boolean
     */
    static function isUrl($url) {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return false;
        } else {
            return true;
        }
    }

    static function getIp() {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
            foreach (explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']) as $ip) {
                $ip = trim($ip);
                return $ip;
            }
        }
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Attempt to make simple template builder
     * @param string $tpl Template name
     * @param string $title Page title
     * return Template code
     */
    static function loadTpl($tpl, $title) {

        echo '
        <!DOCTYPE html>
        <html lang="en">
          <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="">
            <meta name="author" content="">
            <title>'.$title.'</title>
            <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
          </head>

          <body>

            <div class="container">';

        if(file_exists(ROOT . 'includes/tpl/' . $tpl . '.php') === true) {
            include ROOT . 'includes/tpl/' . $tpl . '.php';
        }

        echo '</div>

            <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
            <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
            <script src="' . SITE_R . '/js/scripts.min.js"></script>

            </body>
        </html>
        ';

    }

}
?>
