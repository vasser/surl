<?php
/**
 * Description of Surl
 *
 * @author vasser
 */
class Surl {

    public $_abc = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';

    static function setToken() {
        $token = time();
        $_SESSION['url_token'] = $token;
        return $token;
    }

    function saveUrl($url) {

        $DB = new Mysql();
        $dbLink = $DB->mysqlConnect();

        $enc = $this->shorturl($url);

        $sql = "INSERT INTO `surl_links` (`url`, `surl`) VALUES ('{$url}', '{$enc}')";
        if($dbLink->query($sql)) {

            return SITE_R . $enc;

        } else {
            return false;
        }

    }

    function decodeUrl($surl) {

        $DB = new Mysql();
        $dbLink = $DB->mysqlConnect();

        $sql = "SELECT `id`,`url` FROM `surl_links` WHERE `surl` = '{$surl}' ORDER BY `id` DESC LIMIT 0, 1";

        $result = $dbLink->query($sql);

        if($result && $result->num_rows > 0) {
            $row = $result->fetch_assoc();
            return $row;
        } else {
            return false;
        }

    }

    function checkToken($token) {

        $DB = new Mysql();
        $dbLink = $DB->mysqlConnect();

        $enc = $this->shorturl($url);

        $sql = "SELECT `id` FROM `surl_users` WHERE `token` = '{$token}'";

        $result = $dbLink->query($sql);

        if($result && $result->num_rows > 0) {

            return true;

        } else {
            return false;
        }

    }

    /**
     * Borrowed algorithm, but it is too far from ideal
     * @param type $x
     * @return type
     */
    function code62($x) {
        $show = '';
        while ($x > 0) {
            $s = $x % 62;
            if ($s > 35) {
                $s = chr($s + 61);
            } elseif ($s > 9 && $s <= 35) {
                $s = chr($s + 55);
            }
            $show.=$s;
            $x = floor($x / 62);
        }
        return $show;
    }

    /**
     * Borrowed algorithm, but it is too far from ideal
     * @param type $url
     * @return type
     */
    function shorturl($url) {
        $url = crc32($url);
        $result = sprintf("%u", $url);
        return $this->code62($result);
    }

}
