<?php
/**
 * Description of Stat
 *
 * @author vasser
 */
class Stat {

    private $_ip = null;
    private $_ua = null;
    private $_idLink = null;


    function __construct($ua, $ip, $id_link) {

        $this->_ip = $ip;
        $this->_ua = $ua;
        $this->_idLink = $id_link;

    }


    function getOs() {
        return 'Linux Ubuntu 12.04';
    }

    function getBrowser() {
        return 'Chromium';
    }

    function getLocation() {
        return 'UKR';
    }

    function updateLog() {

        $DB = new Mysql();
        $dbLink = $DB->mysqlConnect();

        $sql = "INSERT INTO `surl_stats_adv` "
                . "(`id_url`,`ip`,`browser`,`os`,`country`)"
                . " VALUES "
                . "('{$this->_idLink}','{$this->_ip}','{$this->getBrowser()}','{$this->getOs()}','{$this->getLocation()}')";

        echo $sql;

        $dbLink->query($sql);

    }

}