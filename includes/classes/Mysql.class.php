<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mysql
 *
 * @author vasser
 */
class Mysql
{

    public $_link = null;

    function __construct() {
        $this->mysqlConnect();
        $this->_link->set_charset(DB_CHAR);
        return $this->_link;
    }

    function mysqlConnect() {
        if($this->_link === null) {
            $this->_link = new mysqli(DB_LOCALHOST, DB_USER, DB_PASSWORD, DB_DATABASE) or 'No connection';
        }
	return $this->_link;
    }

    function mysqlClose(mysqli $db) {
	$db->close();
    }

}


?>
