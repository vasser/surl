<?php
/**
 * Class to register / login users
 *
 * @author vasser
 */

class Login {

    /**
     * Reports about authorise access
     * @var boolean
     */
    public $_Init = false;

    /**
     * Report about login
     * @var string
     */
    public $_LoginLog = null;

    private $_salt = 'L0rem !psum';

    public $_user = array();

    public $_dbLink = null;

    /**
     *
     */
    function __construct() {
        session_start();
        $DB = new Mysql();
        $this->_dbLink = $DB->mysqlConnect();
        if($this->getLoginStatus() === false) {

            if(!is_array($_POST) || empty($_POST)) {
                $this->_Init = false;
            } else {
                $login = Func::getRequestStr($_POST['login']);
                $password = Func::getRequestStr($_POST['password']);
                $name = Func::getRequestStr($_POST['name']);
                $register = Func::getRequestStr($_POST['register']);

                if($register == 'true') {
                    if($this->registerUser($name, $login, $password)) {
                        $this->tryLogin($login, $password);
                        $this->setLoginStatus($login, $password);
                        $this->_Init = true;
                    } else {
                        $this->_Init = false;
                        $this->_LoginLog = "Error happened. Please try again";
                    }
                } else {
                    if($this->tryLogin($login, $password)) {
                        $this->setLoginStatus($login, $password);
                        $this->_Init = true;
                    } else {
                        $this->_Init = false;
                        $this->_LoginLog = "Login / password incorrect. Please try again";
                    }
                }
            }
        } else {
            $this->_Init = true;
        }
    }

    /**
     *
     * @return type
     */
    function getInit() {
        return $this->_Init;
    }

    function hashPass($password) {
        return md5($password);
    }

    function hashLogin($login, $password) {
        return md5($login) . md5($password) . md5($this->_salt);
    }

    /**
     *
     * @param type $login
     * @param type $password
     * @return type
     */
    function setToken($login, $password) {
        return md5($login . $password);
    }

    function tryLogin($login, $password) {
        $DB = $this->_dbLink;
        $sql = "SELECT `id`,`name`,`token` FROM `surl_users` WHERE `email` = '{$login}' AND `pass` = '{$this->hashPass($password)}'";
        $result = $DB->query($sql);
        if($result && $result->num_rows > 0) {
            $this->_user = $result->fetch_assoc();
            return true;
        } else {
            return false;
        }
    }

    function registerUser($name, $login, $password) {
        $DB = $this->_dbLink;
        $sql = "INSERT INTO `surl_users`"
                . "(`name`,`email`,`pass`,`token`)"
                . " VALUES "
                . "('{$name}','{$login}','{$this->hashPass($password)}','{$this->setToken($login, $password)}')";

        $result = $DB->query($sql);

        if($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param type $key User ID in credentials array
     */
    function setLoginStatus($login, $password) {

        $hash = $this->hashLogin($login, $password);

        $_SESSION['userLogin'] = '1';
        $_SESSION['userLoginData'] = $hash;

    }

    /**
     *
     * @return boolean
     */
    function getLoginStatus() {

        if($_SESSION['userLogin'] == '1') {
            $DB = $this->_dbLink;
            $sql = "SELECT `id`,`name`,`token` FROM  `surl_users` "
                    . "WHERE CONCAT( MD5(  `email` ) , MD5(  `password` ) , MD5(  '{$this->_salt}' ) ) = "
                    . "'{$_SESSION['userLoginData']}'";
            $result = $DB->query($sql);
            if($result && $result->num_rows > 0) {
                $this->_user = $result->fetch_assoc();
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }

}
