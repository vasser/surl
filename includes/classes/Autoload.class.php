<?php
/**
 * Class Autoload useing for automatic classes loading
 * @author vasser
 */
class Autoload {
    /**
     * @var array Libraries classes array
     */
    static protected $_paths = array();

    /**
     * @var array Class maps
     */
    static protected $_classMap = array();

    /**
     * Register path to the classes dir
     * @param string $path Path to the library
     */
    static public function registerPath($path)
    {
        if (!in_array($path, self::$_paths))
            self::$_paths[] = $path;
    }

    /**
     * Load class
     * @param string $class Class name
     * @return boolean
     */
    public static function load($class)
    {
        if (!empty(self::$_classMap) && array_key_exists($class, self::$_classMap[$class])) {
            require_once self::$_classMap[$class];
            return true;
        }

        $file = $class . '.class.php';

        foreach (self::$_paths as $path) {
            if (file_exists($path . DIRECTORY_SEPARATOR . $file)) {
                require_once $path . DIRECTORY_SEPARATOR . $file;
                return true;
            }
        }
        return false;
    }

    /**
     * Include classes map
     * @param string $path Map path
     */
    static public function loadMap($path)
    {
        if (!file_exists($path)) {
            self::$_classMap = array();
            return;
        }
        self::$_classMap = include($path);
    }
}
?>
